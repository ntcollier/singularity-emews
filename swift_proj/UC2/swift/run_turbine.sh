#!/bin/bash

set -eu

if [ "$#" -ne 1 ]; then
  script_name=$(basename $0)
  echo "Usage: ${script_name} EXPERIMENT_ID (e.g. ${script_name} experiment_1)"
  exit 1
fi

# USER SETTINGS
export PROCS=${PROCS:-4}
INSTANCE_COUNT=${INSTANCE_COUNT:-8}
# END USER SETTINGS

# uncomment to turn on swift/t logging. Can also set TURBINE_LOG,
# TURBINE_DEBUG, and ADLB_DEBUG to 0 to turn off logging
# export TURBINE_LOG=1 TURBINE_DEBUG=1 ADLB_DEBUG=1
export EMEWS_PROJECT_ROOT=$( cd $( dirname $0 )/.. ; /bin/pwd )
# source some utility functions used by EMEWS in this script
source "${EMEWS_PROJECT_ROOT}/etc/emews_utils.sh"

export EXPID=$1
export TURBINE_OUTPUT_ROOT=${TURBINE_OUTPUT_ROOT:-$EMEWS_PROJECT_ROOT/experiments}
export TURBINE_OUTPUT=$TURBINE_OUTPUT_ROOT/$EXPID
export TURBINE_JOBNAME="${EXPID}_job"

# if R cannot be found, then these will need to be
# uncommented and set correctly.
# export R_HOME=/path/to/R
# export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$R_HOME/lib

export LD_LIBRARY_PATH="/usr/lib/python3.5/config-3.5m-x86_64-linux-gnu"
EQPY=/opt/emews/EQ-Py
export PYTHONPATH=$EMEWS_PROJECT_ROOT/python:$EQPY

# Resident task workers and ranks
export TURBINE_RESIDENT_WORK_WORKERS=1
export RESIDENT_WORK_RANKS=$(( PROCS - 2 ))

NI=${NUM_ITER:-3}
NV=${NUM_VAR:-1}
NP=${NUM_POP:-3}

CMD_LINE_ARGS="$* -ni=$NI -nv=$NV -np=$NP -r=0"

# Uncomment this for the BG/Q:
#export MODE=BGQ QUEUE=default

# set machine to your schedule type (e.g. pbs, slurm, cobalt etc.),
# or empty for an immediate non-queued unscheduled run
MACHINE=""

if [ -n "$MACHINE" ]; then
  MACHINE="-m $MACHINE"
fi

# Add any script variables that you want to log as
# part of the experiment meta data to the USER_VARS array,
# for example, USER_VARS=("VAR_1" "VAR_2")
USER_VARS=()
# log variables and script to to TURBINE_OUTPUT directory
log_script

# echo's anything following this to standard out
set -x
SWIFT_FILE=workflow.swift
TIC_FILE_NAME=$(echo $SWIFT_FILE | sed "s/\.swift/\.tic/")
TIC_OUTPUT=$TURBINE_OUTPUT/$TIC_FILE_NAME

turbine -n $PROCS $MACHINE \
 	-e TURBINE_RESIDENT_WORK_WORKERS=$TURBINE_RESIDENT_WORK_WORKERS \
  -e RESIDENT_WORK_RANKS=$RESIDENT_WORK_RANKS \
  -e EMEWS_PROJECT_ROOT=$EMEWS_PROJECT_ROOT \
  -e TURBINE_OUTPUT=$TURBINE_OUTPUT \
  -e PYTHONPATH=$PYTHONPATH \
  -e LD_LIBRARY_PATH=$LD_LIBRARY_PATH \
  $TIC_OUTPUT $CMD_LINE_ARGS
