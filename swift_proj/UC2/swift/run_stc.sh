#! /usr/bin/env bash

set -eu

if [ "$#" -ne 1 ]; then
  script_name=$(basename $0)
  echo "Usage: ${script_name} EXPERIMENT_ID (e.g. ${script_name} experiment_1)"
  exit 1
fi

export EMEWS_PROJECT_ROOT=$( cd $( dirname $0 )/.. ; /bin/pwd )
export EXPID=$1
export TURBINE_OUTPUT=$EMEWS_PROJECT_ROOT/experiments/$EXPID
mkdir -p $TURBINE_OUTPUT

export PATH=/opt/swift-t/stc/bin:$PATH

# EQ/Py location
EQPY=/opt/emews/EQ-Py
# R_utils location
R_UTILS=$EMEWS_PROJECT_ROOT/swift


# echo's anything following this to standard out
set -x
SWIFT_FILE=workflow.swift
TIC_FILE_NAME=$(echo $SWIFT_FILE | sed "s/\.swift/\.tic/")
TIC_OUTPUT=$TURBINE_OUTPUT/$TIC_FILE_NAME

stc -p  -I $EQPY -r $EQPY -I $R_UTILS -r $R_UTILS \
  $EMEWS_PROJECT_ROOT/swift/$SWIFT_FILE $TIC_OUTPUT
