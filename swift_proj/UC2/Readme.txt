UC2 Readme.txt

* This is the runnable source code for the UC2 tutorial.

* See the EMEWS tutorial website at http://www.mcs.anl.gov/~emews/tutorial/
  for more information.

* Prior to running the UC2 workflow, the complete_model.jar must be 
  unzipped into a complete_model directory. From the command line:

	unzip complete_model.jar -d complete_model/
