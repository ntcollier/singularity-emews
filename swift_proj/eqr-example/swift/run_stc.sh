#! /usr/bin/env bash

set -eu

if [ "$#" -ne 1 ]; then
  script_name=$(basename $0)
  echo "Usage: ${script_name} EXPERIMENT_ID (e.g. ${script_name} experiment_1)"
  exit 1
fi

export EMEWS_PROJECT_ROOT=$( cd $( dirname $0 )/.. ; /bin/pwd )
export EXPID=$1
export TURBINE_OUTPUT=$EMEWS_PROJECT_ROOT/experiments/$EXPID
mkdir -p $TURBINE_OUTPUT

export PATH=/opt/swift-t/stc/bin:$PATH

# EQ/R location
EQR=/opt/emews/EQ-R


# echo's anything following this to standard out
set -x
SWIFT_FILE=swift_run_eqr.swift
TIC_FILE_NAME=$(echo $SWIFT_FILE | sed "s/\.swift/\.tic/")
TIC_OUTPUT=$TURBINE_OUTPUT/$TIC_FILE_NAME

stc -p  -I $EQR -r $EQR $EMEWS_PROJECT_ROOT/swift/$SWIFT_FILE $TIC_OUTPUT
