import io;
import sys;
import files;
import location;
import string;
import EQR;
import R;
import assert;

string emews_root = getenv("EMEWS_PROJECT_ROOT");
string turbine_output = getenv("TURBINE_OUTPUT");
string resident_work_ranks = getenv("RESIDENT_WORK_RANKS");
string instance_count = argv("instance_count");
string r_ranks[] = split(resident_work_ranks,",");

app (file out, file err) run_model (string model_sh, string param_line, string instance)
{
    "bash" model_sh param_line emews_root instance @stdout=out @stderr=err;
}

(float result) obj(string param_line, string id_suffix)
{
    // make instance dir
    string instance_dir = "%s/instance_%s/" % (turbine_output, id_suffix);
    make_dir(instance_dir) => {
      file out <instance_dir + "out.txt">;
      file err <instance_dir + "err.txt">;
      string model_sh = emews_root + "/scripts/eqr_write.sh";
      (out,err) = run_model(model_sh, param_line,instance_dir) =>
      result = 0;

    }
}

(void v) loop(location ME, int ME_rank, int trials) {

    for (boolean b = true, int i = 1;
       b;
       b=c, i = i + 1)
  {
    string params =  EQR_get(ME);
    boolean c;

    // TODO
    // Edit the finished flag, if necessary.
    // when the python algorithm is finished it should
    // pass "DONE" into the queue, and then the
    // final set of parameters. If your python algorithm
    // passes something else then change "DONE" to that
    if (params == "DONE")
    {
      string finals =  EQR_get(ME);
      string fname = "%s/final_result_%i" % (turbine_output, ME_rank);
      file results_file <fname> = write(finals) =>
      printf("Writing final result to %s", fname) =>
      // printf("Results: %s", finals) =>
      v = make_void() =>
      c = false;
    }
    else if (params == "EQR_ABORT")
    {
      printf("EQR aborted: see output for R error") =>
      string why = EQR_get(ME);
      printf("%s", why) =>
      v = propagate() =>
      c = false;
    }
    else
    {

        printf("params: %s", params);
        string param_array[] = split(params, ";");
        float results[];
        foreach p, j in param_array
        {
            results[j] = obj(p, "%i_%i_%i" % (ME_rank,i,j));
        }

        string rs[];
        foreach result, k in results
        {
            rs[k] = fromfloat(result);
        }
        string res = join(rs, ",");
        EQR_put(ME, res) => c = true;

    }
  }
}

(void o) start(int ME_rank, int num_variations, int random_seed) {
    location ME = locationFromRank(ME_rank);
    string algorithm = strcat(emews_root,"/R/algorithm.R");
    EQR_init_script(ME, algorithm) =>
    EQR_get(ME) =>
    EQR_put(ME, instance_count) =>
    loop(ME, ME_rank, num_variations) => {
        EQR_stop(ME) =>
        EQR_delete_R(ME);
        o = propagate();
    }
}

// deletes the specified directory
app (void o) rm_dir(string dirname) {
  "rm" "-rf" dirname;
}

// call this to create any required directories
app (void o) make_dir(string dirname) {
  "mkdir" "-p" dirname;
}

// anything that need to be done prior to a model runs
// (e.g. file creation) can be done here
//app (void o) run_prerequisites() {
//
//}

main() {

  // TODO
  // Retrieve arguments to this script here
  // these are typically used for initializing the R algorithm
  // Here, as an example, we retrieve the number of variations
  // (i.e. trials) for each model run, and the random seed for the
  // R algorithm.
  int num_variations = toint(argv("nv", "1"));
  int random_seed = toint(argv("seed", "0"));

  assert(strlen(emews_root) > 0, "Set EMEWS_PROJECT_ROOT!");

  int ME_ranks[];
  foreach r_rank, i in r_ranks{
    ME_ranks[i] = toint(r_rank);
  }

  //run_prerequisites() => {
    foreach ME_rank, i in ME_ranks {
      start(ME_rank, num_variations, random_seed) =>
      printf("End rank: %d", ME_rank);
    }
//}
}
