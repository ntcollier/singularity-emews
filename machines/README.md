# Building the Singularity Image #

* Copy `base/singularity-emews.def` and edit appropriately
* singularity create --size 5000 X.img
* sudo singularity bootstrap X.img X.def


# Running #
Compile with:
* singularity exec emews.img X/swift/stc_eqr.sh ST1
Run with:
* mpiexec -n 4 singularity exec emews.img X/swift/run_turbine.sh ST1

Latter part is the app execution part of a submit type script if running on
an schedule system.

X should be somewhere in home directory or in a mounted directory
