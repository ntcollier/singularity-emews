#! /usr/bin/env bash
set -eu

if [ "$#" -ne 1 ]; then
  script_name=$(basename $0)
  echo "Usage: ${script_name} machine_directory (e.g. ${script_name} bebop)"
  exit 1
fi

ROOT=$( cd $( dirname $0 ); /bin/pwd )
MACHINE_NAME=$1
MACHINE_DIRECTORY=$ROOT/$MACHINE_NAME

if [[ ! -d $MACHINE_DIRECTORY ]]; then
  echo "Machine directory ${MACHINE_DIRECTORY} not found."
  exit 1
fi

cd $MACHINE_DIRECTORY
IMAGE_NAME=singularity-emews-$MACHINE_NAME.img
DEF_NAME=singularity-emews-$MACHINE_NAME.def
rm -f $IMAGE_NAME
singularity create --size 5000 $IMAGE_NAME
sudo singularity bootstrap $IMAGE_NAME $DEF_NAME
