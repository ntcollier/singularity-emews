#! /usr/bin/env bash

set -eu

# module load singularity fails without this
TCLSH=/usr/bin/tclsh

module load singularity

THIS=$( cd $( dirname $0 ); /bin/pwd )
PROJECT_ROOT=$( cd $( dirname $0 )/../.. ; /bin/pwd )

if [ $1 == "eqr" ]; then
	singularity exec -B $PROJECT_ROOT:/singularity_emews singularity-emews-midway2.img  /singularity_emews/swift_proj/eqr-example/swift/run_stc.sh $2
	sbatch $THIS/eqr_example.sbatch $PROJECT_ROOT $2

elif [ $1 == "uc2" ]; then

	if [ ! -d "${PROJECT_ROOT}/swift_proj/UC2/complete_model" ]; then
		unzip $PROJECT_ROOT/swift_proj/UC2/complete_model.jar -d $PROJECT_ROOT/swift_proj/UC2/complete_model/
	fi

	singularity exec -B $PROJECT_ROOT:/singularity_emews singularity-emews-midway2.img /singularity_emews/swift_proj/UC2/swift/run_stc.sh $2
	sbatch $THIS/uc2_example.sbatch $PROJECT_ROOT $2

else
	echo "Invalid argument: must be one of eqr or uc2"
fi
