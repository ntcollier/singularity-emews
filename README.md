# Swift/T and EMEWS in a Singularity Container #

Requirements:

  * Singularity 2.3.2 - http://singularity.lbl.gov/release-2-3-2

The machines directory contains code for creating and running examples on
specific resources such as Midway 2 and Bebop. To create a singularity
container for one of these resources use the `machines/create_image.sh`
passing it the appropriate sub-directory. For example, `./create_image.sh bebop`
to create an image appropriate for Bebop.

You can also get the images from here:

https://anl.box.com/s/jobwbl8ag35n14hxmsqnpssssy03pay4

The essential part of creating the image is to match the mpi implementation
and version between the container and the host. Singularity recommends
openmpi 2.1+. See, http://singularity.lbl.gov/docs-hpc.

The swift_proj directory contains two example EMEWS projects: a simple example
that exercises EQ/R and the UC2 example from the EMEWS tutorial that uses
EQ/Py and the deap GA library. The individual machine directories (e.g
machines/bebop) contain a `submit.sh` script that can be used to run the
examples in singularity containers.

```
submit.sh
  Usage: submit.sh eqr | uc2 experiment_id
```
For example `submit.sh eqr ST1 ` to run the the eqr example in the ST1 directory.
Currently, std err and out are written to a file in the logs directory (e.g.
machines/bebop/logs), but the experiment output is in the typical EMEWS
experiment directory (e.g. swift_proj/eqr-example/experiments/ST1).

`submit.sh` assumes that the appropriate singularity image is in the
corresponding machines directory. So, the image needs to be copied to there.

The submit script does two things it compiles the swift script into the
experiment directory by calling a script to do that, passing it the
experiment directory. For example,

`singularity exec singularity-emews-midway2.img swift_proj/eqr-example/swift/run_stc.sh ST1`

And it then submits an sbatch script that ultimate runs turbine and the compiled
swift scipt. Again by calling a script to do that.

`mpirun singularity exec singularity-emews-midway2.img  swift_proj/eqr-example/swift/run_turbine.sh ST1`
